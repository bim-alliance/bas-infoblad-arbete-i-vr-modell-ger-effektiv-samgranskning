![Bild 1](media/1.jpeg)

**Istället för att enbart bli ett presentationsprojekt har VR-modellen blivit ett verktyg in i projektet. Ett nytt framgångsrikt arbetssätt har utvecklats.**

# Arbete i VR-modell ger effektiv samgranskning

> ##### I arbetet med att ta fram en arbetsplan för Förbifart Stockholm har Trafikverket arbetat med VRmodeller. Erfarenheterna är mycket positiva och i nästa skede av Förbifart Stockholm-projektet, som handlar om att arbeta fram bygghandlingar, kommer BIM att integreras i arbetet. 

 VI HAR LÄNGTAT EFTER ATT PROVA BÅDE VR OCH BIM och satsa på ett annat arbetssätt än det gängse. I Förbifart Stockholm har vi haft den möjligheten och vi kan redan nu tala om en framgångssaga,
säger Anders Wengelin, vägutformningsansvarig för Förbifart Stockholm-projektet på Trafikverket.
I samband med projekteringen av Norra länken försökte dåvarande Vägverket ta steget in i 3Dvärlden men av upphandlingsjuridiska skäl misslyckades detta delvis. När så projekteringen av Förbifart Stockholm skulle starta 2007 var man på Vägverkets tekniska avdelning överens om att det här handlade om ett så stort projekt, cirka 28 miljarder, att det fanns möjligheter att satsa utvecklingspengar på löpande räkning.
När konsulttjänsterna handlades upp togs VR med från dag ett i kontrakten. Kontrakt skrevs med ett konsortium som bestod av Sweco, Thyréns och WSP. I avtalet med konsortiet gjordes
VR till ett eget teknikområde likvärdigt med exempelvis geoteknik och brobyggnad.
​	– Man skulle göra en form av VR-modell åt oss. Resultatet blev så bra att vi använde den som en del i processen. Därmed blev VR mer process än produkt, säger Anders Wengelin. I ett megaprojekt som Förbifart Stockholm är väldigt många människor och teknikområden inblandade. Traditionellt
arbetar man ganska mycket med ritningar, i bästa fall i datafilsformat, och texter. Arbetet med att skicka papper mellan varandra är gigantiskt. VR-modellen innebar nya lösningar. Istället för att bli ett presentationsverktyg för att visa Förbifart Stockholm utåt blev den ett verktyg in i projektet. Istället för att komma till möten med ritningsrullar hade alla inblandade sett till att ha levererat aktuell information till dem som jobbade med VR-modellen.
​	– Under tre års tid har vi levt med en halvfärdig modell, säger Anders Wengelin. Om vissa saker inte varit färdigritade så har det varit ett hål i modellen. Vägutformare, landskapsarkitekter, brobyggare och alla andra har kunnat sitta tillsammans och vrida och vända på modellen och diskutera hålet och förslag
på utformning. Alla har kunnat se problemen samtidigt och vi har fått en konstruktiv diskussion och dialog. Istället för att var och en sitter på sin kammare och skickar datafiler eller pappersritningar till varandra, så har vi jobbat mot VR-modellen som en arbetsmodell. Där ser jag den stora nyttan i vårt nya arbetssätt.
​	I Norra Länken var arbetet med arbetsplanen till stora delar ritningsbaserat och i 2D. VR handlades upp separat som en ren presentation ut mot tredje man. Där blev VR något man skapade på slutet för att kunna visa upp hur det blev. 

I NORMALA FALL SKER KVALITETSSÄKRING av arbetsplanen på olika nivåer. Egenkontroll, samordningskontroll, oberoende granskning följs av att beställaren gör en mottagningskontroll. För
ett projekt som Förbifart Stockholm handlar det om ett par tre hundra möten för dessa kontroller.
​	Egenkontrollen måste fortfarande göras men tack vare VRmodellen kunde samgranskning, oberoende granskning och mottagningskontroll slås ihop. Projektet delades in i geografiska delar och granskning gjordes för varje avgränsad del. Det blev tio granskningsmöten. Alla inblandade deltog och kunde reagera och direkt ta en diskussion. 
​	Ett orosmoment var om man kunde lita på att VR-modellen visar samma saker som sedan visas på ritningarna. En hel del stickprov gjordes som visade att det fungerade som tänkt. Villkoret för dem som arbetade med VR-modellen var att de inte fick putsa till något för att det skulle se snyggt ut. Fel i projekteringen skulle synas direkt i modellen.

NU NÄR ARBETSPLANEN I PRINCIP ÄR FÄRDIG vidtar nästa steg med att ta fram specifika tekniska handlingar för bygget. Efter ny upphandling är nya konsulter utsedda.
​	– I bygghandlingsskedet kan vi gå från VR-världen till BIMvärlden. VR-modellen är en mycket bra plattform att stå på inför det arbetet. I upphandlingen har vi ställt krav på två olika BIM-modeller – en samgranskningsmodell och en visualiseringsmodell. Den tekniska modellen kan leva sitt liv i respektive
delområde men visualiseringen måste vara uniform över hela projektet. Vi på beställarsidan vill själva ordentligt kunna se hur helheten ser ut och kunna visa upp den för tredje part.
​	Trafikverket har inte bara använt och haft nytta av VR-modellen internt inom projektet utan även vid kontakter med andra aktörer som SL, kommuner och Räddningstjänsten. Här har VR-modellen medfört en mycket smidigare process jämfört med tidigare. Genom att direkt kunna visa hur det kommer att se ut blir ingångsvinkeln i samtalet en annan. För att förstå en teknisk ritning måste man näst intill vara ingenjör men en VR-modell kan förstås av vem som. 

EN SVÅRIGHET MED VR-ARBETE ÄR tidsfördröjningen. Anders Wengelin skulle vilja ha modellen uppdaterad varje dag i varenda detalj vilket inte går. Varje projektör har sitt projekteringssystem som man är van vid och vill arbeta med. Alla levererar till VR-gruppen som behöver ett par dagar för att få
in ändringar och tillägg i modellen och sammanställa allt till en helhet.
​	– Just nu finns originalen hos respektive teknikområde och VR är en avbildning av dessa med någon veckas fördröjning. Självklart ska alla kunna leverera direkt till VR-modellen som
då blir originalet men där befinner vi oss inte idag. Detta är det viktigaste som behöver utvecklas men det ligger nog fem-tio år fram i tiden.

ETT ANNAT PROBLEM ÄR ATT ARBETET MED VR kostar mycket pengar. Jämfört med andra arbetsplaner så har detta blivit en väldigt dyr arbetsplan. Men Anders Wengelin frågar sig hur mycket pengar man sparat genom att undvika fel som kunde uppstått om man gjort på annat sätt. Ingen har facit men han är ganska övertygad om att man i VR-processen hittat en massa fel som annars skulle ha missats vid granskningarna. Kanske sparas merkostnaden i arbetsplanen in under byggtiden men
det kommer ingen att kunna värdera. 
​	– Kostnaden för en arbetsplan i VR-format kan se dyr ut men utgör ändå en liten del av den totala byggkostnaden i ett så här stort projekt. Vi ser detta som utvecklingspengar. Samtidigt har det förändrade arbetssättet skapat en entreprenörsanda, en klondykestämning, med mycket engagerade medarbetare. Det är värt mycket.

Fotnot: VR står för Virtual Reality.